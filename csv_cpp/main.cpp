/*
 * Source: https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c/14266139#14266139
 */

#include <iostream>
#include <string>

int main(int argc, char **argv) {

    std::cout << "Init..." << std::endl;

    if (argc < 3) {
        std::cout << "Run csv_cpp.exe <csv_text_to_parse> <field_delimitator>" << std::endl;
        return 1;
    }

    std::string s (argv[1]);
    std::string delim = (argv[2]);

    size_t start = 0U;
    size_t end = s.find(delim);
    while (end != std::string::npos)
    {
        std::cout << s.substr(start, end - start) << std::endl;
        start = end + delim.length();
        end = s.find(delim, start);
    }

    std::cout << s.substr(start, end);

    return 0;
}