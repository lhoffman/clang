/*
 * func_pointers.c
 * 
 * How to create and call an array of function pointer.
 * 
 * References:
 *   - https://stackoverflow.com/questions/252748/how-can-i-use-an-array-of-function-pointers
 *   - http://cs-fundamentals.com/c-programming/function-pointers-in-c.php
 * 
 * To build              : gcc -Wall -Werror func_pointers.c -o func_pointers.exe 
 * To build for debugging: gcc -Wall -Werror -g func_pointers.c -o func_pointers.exe 
 * To run                : func_pointers.exe 5
 */

#define FN_PTR_ARR 10

#include <stdio.h>
#include <stdlib.h>

// Declaration of an array of function pointers called validators
int (*validators[FN_PTR_ARR])(int);

// Create the type PtrValidator to refer to a function pointer
typedef int (*PtrValidator)(int);

// Function prototypes
int isLessThanZero(int);
int isGreaterThanZero(int);
int isEqualToZero(int);
int isEven(int);
int isOdd(int);

// check function will receive an int and an array of type PtrValidator
void check(int, PtrValidator[FN_PTR_ARR]);

/***************************************************************************
 *     MAIN FUNCTION 
 ***************************************************************************/ 
int main(int argc, char **argv) 
{
    int num = 0;
    PtrValidator ptrValidators[FN_PTR_ARR] = {0};

    if (argc > 1) {
        num = atoi(argv[1]);
    }

    printf("=================================================\n");
    printf(" Set fp array and loop it to call each function. \n");
    printf("=================================================\n");

    validators[0] = isLessThanZero;
    validators[1] = isGreaterThanZero;
    validators[2] = isEqualToZero;
    validators[3] = isEven;
    validators[4] = isOdd;

    for(int i = 0; i < FN_PTR_ARR; i++) {
        if (validators[i] != NULL) {
            printf("Number %d - Function %d -> %d\n", num, i+1, 
                   validators[i](num));
        } else {
            printf("Function slot %d empty!\n", i+1);
        }
    }

    printf("=================================================\n");
    printf(" Create and set a fp type and call the check()   \n");
    printf(" function passing the type as parameter.         \n");
    printf("=================================================\n");
    
    ptrValidators[0] = isLessThanZero;
    ptrValidators[1] = isGreaterThanZero;
    ptrValidators[2] = isEqualToZero;
    ptrValidators[3] = isEven;
    ptrValidators[4] = isOdd;
    check(num, ptrValidators);

    return 0;
}

/***************************************************************************
 * 
 *     PROTOTYPE IMPLEMENTATIONS
 * 
 ***************************************************************************/

void check(int num, PtrValidator ptrValidators[FN_PTR_ARR])
{
    for(int i = 0; i < FN_PTR_ARR; i++) {
        if (ptrValidators[i] != NULL) {
            printf("Number %d - Function %d -> %d\n", num, i+1, 
                   ptrValidators[i](num));
        } else {
            printf("Function slot %d empty!\n", i+1);
        }
    }
}

int isLessThanZero(int n)
{
    if (n < 0)
        return 1;
    
    return 0;
}

int isGreaterThanZero(int n)
{
    if (!isLessThanZero(n) && !isEqualToZero(n))
        return 1;

    return 0;
}

int isEqualToZero(int n)
{
    if (n == 0)
        return 1;

    return 0;
}

int isEven(int n)
{
    if (n%2 == 0)
        return 1;

    return 0;
}

int isOdd(int n)
{
    if (!isEven(n))
        return 1;

    return 0;
}