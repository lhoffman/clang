/*
 * Example code from : http://www.cplusplus.com/faq/sequences/strings/split/
 */

#include <stdio.h>
#include <string.h>
#include "c_tokenizer.h"

int main(int argc, char **argv)
{
    char buffer[2000];

    if (argc < 2) {
        return 1;
    }

    strcpy(buffer, argv[1]);
    
    //const char *s = ",,a,,b,,"; /* see notes with accompanying text below */

    tokenizer_t tok = tokenizer(buffer, ",", TOKENIZER_EMPTIES_OK);
    const char *token;
    unsigned n;

    n = 0;
    for (token = tokenize(&tok); token; token = tokenize(&tok))
    {
        printf("\"%s\"\n", token);
        n += 1;
    }
    free_tokenizer(&tok);
    printf("%u tokens\n", n);

    return 0;
}