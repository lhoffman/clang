#include "main.h"

int main(int argc, char **argv)
{
	CfgMatrix cfg;

	int size = (int)(sizeof(cfg) / sizeof(cfg[0]));

	printf("sizeof CfgMatrix %d(%d)\n\n",size, MAX_SIZE);

	for (int i = 0; i < size; i++) {
		cfg[i] = i + 10;
	}

	for (int i = 0; i < MAX_SIZE; i++) {
		printf("cfg index %d, value %d\n",i, cfg[i]);
	}

	return 0;
}
