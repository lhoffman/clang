/* 
 * recursive_call.c
 * 
 * Create an array of ints, then execute a recursive function that decrement 
 * by 1 each element of the array. Then print it.
 */

#include <stdio.h>

int calc(int *, int);

int main(int argc, char **argv) {
	int i = 0;
	int x[10] = {10,20,30,40,50,60,70,80,90,100};

	printf("%d", calc(x, 10));	

	printf("\n\n");

	for(i = 0 ; i < 10 ; i++) {
		printf("=== main: %d\n", x[i]);
	}

}

int calc(int *a, int l) {

	// Base case to return to caller function
	if (l <= 0) {
		return l;
	}

	l--;
	a[l] = a[l] - 1;
	printf("=== func: %d\n", a[l]);
	return calc(a, l);
}
