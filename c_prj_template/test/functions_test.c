#include <stdio.h>
#include <functions_test.h>

void test_functions()
{
        int x,y,r;

        x = 1;
        y = 1;
        r = 2;

        assert(add(x,y) == r);

        x = -1;
        y = 1;
        r = 0;

        assert(add(x,y) == r);

        x = 0;
        y = 0;
        r = 0;

        assert(add(x,y) == r);

        x = 0;
        y = -1;
        r = -1;

        assert(add(x,y) == r);

        x = 1;
        y = 1;
        r = 0;

        assert(subtract(x,y) == r);

        x = -1;
        y = 1;
        r = -2;

        assert(subtract(x,y) == r);
 
        x = 0;
        y = 0;
        r = 0;

        assert(subtract(x,y) == r);

        x = 0;
        y = -1;
        r = 1;

        assert(subtract(x,y) == r);        

        x = 1;
        y = 1;
        r = 1;

        assert(multiply(x,y) == r);

        x = -1;
        y = 1;
        r = -1;

        assert(multiply(x,y) == r);
 
        x = 0;
        y = 0;
        r = 0;

        assert(multiply(x,y) == r);

        x = 0;
        y = -1;
        r = 0;

        assert(multiply(x,y) == r);                

        x = 1;
        y = 1;
        r = 1;

        assert(divide(x,y) == r);

        x = -1;
        y = 1;
        r = -1;

        assert(divide(x,y) == r);
 
        x = 0;
        y = 0;
        r = 0;

        assert(divide(x,y) == r);

        x = 0;
        y = -1;
        r = 0;

        assert(divide(x,y) == r);                        
}