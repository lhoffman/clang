#include <stdio.h>
#include <../test/functions_test.h>
#include <../include/functions.h>

int main(int argc, char **argv) {

        int a,s,m,d;

	printf("Init World...\n");
        printf("Strating Unit Tests...\n");
        printf("Unit Tests Finished...\n");
        test_functions();
        printf("Start Process...\n");

        a = add(123,123);
        s = subtract(a,123);
        m = multiply(s,123);
        d = divide(m,123);

        printf("\tAdding     : %d\n", a);
        printf("\tSubtracting: %d\n", s);
        printf("\tMultiplying: %d\n", m);
        printf("\tDividing   : %d\n", d);

        return 0;
}