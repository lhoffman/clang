#include <stdio.h>
#include <stdlib.h>

void switch_adv();

int main(int argc, char** argv) {

	int n = 0;

	printf("=========================\n");
	printf("Switch/Case statment test\n");
	printf("=========================\n\n");

	if (argc > 1) {
		n = atoi(argv[1]);
	}

	switch(n) {	
	case 1:
		printf("First Test  - Value of int is %d.\n", n);
		break;
	case 2:
		printf("Second Test - Value of int is %d.\n", n);
		break;
	case 3:
		printf("Third Test - Value of int is %d. (no break)\n", n);
	default:
		printf("Default Test.\n");
	case 4:
		printf("Fourth Test - Value of int is %d.\n", n);
		break;
	case 5:
		printf("Fifth Test - Value of int is %d. (no break)\n", n);	
	}

	return 0;
}

/*
 * Case should be a integer constant no logical operation is allowed
 *

void switch_adv() 
{
	int nums[5];

	nums[0] = 0;
	nums[1] = 1;
	nums[2] = 0;
	nums[3] = 1;
	nums[4] = 0;

	for (int i = 0; i < 5; i++) {

		switch(i) {
		case 0 && nums[i] > 0:
			printf("i = 0 && nums[i] > 0\n");
			break;
		case 1 && nums[i] > 0:
			printf("i = 1 && nums[i] > 0\n");
			break;
		case 2 && nums[i] > 0:
			printf("i = 2 && nums[i] > 0\n");
			break;
		case 3 && nums[i] > 0:
			printf("i = 3 && nums[i] > 0\n");
			break;
		case 4 && nums[i] > 0:
			printf("i = 4 && nums[i] > 0\n");
			break;
		}
	}
}

 *
 *
 */
